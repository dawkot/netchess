import asyncnet, asyncdispatch, unittest, options, sequtils, future, sequtils, strutils, os, future, times, typetraits, msgpack4nim, tables, pipe
import rpg_net_messages, rpg_net_utils, rpg_net_universal, utils

type
    Client = ref object
        socket:     AsyncSocket
        address:    string
        id:         ClientId

        messageFuture:  Future[string]

        room:       Option[Room]

    MockupClient = ref object
        socket: AsyncSocket

    Room = ref object
        clients:    seq[Client]
        chat:       Chat
    
    Message = object
        author:     Client
        content:    string
        time:       Time

    Chat = ref object
        messages:   seq[Message]

var
    serverSocket:   AsyncSocket
    clients:        seq[Client]
    mockups:        seq[MockupClient]

    port:           Port
    address:        string
    connection:     Future[tuple[address: string, client: AsyncSocket]]

    rooms:          seq[Room]

#boilerplate

proc `==`(a, b: ClientId): bool {.borrow.}

#rpcs

proc sayOnChat(c: Client, content: string) =
    if c.room.isNone: return
    c.room.get.chat.messages.add Message(author: c, content: content, time: getTime())

proc sayToClient(c: Client, targetId: ClientId, content: string) =
    for c in clients:
        if c.id == targetId: 
            #asyncCheck c.socket.send(asMsgPack(("messageFromClient", c.id.int, content)))
            break

#room

proc newRoom: Room =
    let r = Room(clients: @[], chat: Chat(messages: @[]))
    rooms.add(r)
    r

proc removeRoom(r: Room) =
    for c in r.clients: c.room = none Room
    let i = rooms.findi((rr: Room) => rr == r)
    rooms.del i

proc enter(c: Client, r: Room) =
        c.room = some(r)
        r.clients.add(c)

#server

proc initServer(port: Port, address = ""): Server =
    serverSocket = newAsyncSocket()
    serverSocket.setSockOpt(OptReuseAddr, true)
    serverSocket.bindAddr(port, address)
    connection = serverSocket.acceptAddr()
    clients = @[]
    rpg_net_server.port = port
    rpg_net_server.address = address
    mockups = @[]
    rooms = @[]

proc startAcceptingConnections = serverSocket.listen
proc stopAcceptingConnections = serverSocket.close

proc remove(c: Client) =
    clients.del c
    if c.room.isSome:   c.room.get.clients.del c

proc sendTo(c: Client, msg: SomeNetMsgArgs) {.async.} =
    c.socket.send packNetMsg msg

proc processMessage(c: Client, msg: string) =
    var unpacked: tuple[kind: NetMsgKind, argsPacked: string]
    msg.unpack unpacked

    case unpacked.kind
    of clientSayOnChat:         
        var args: tuple[kind: NetMsgKind, argsPacked: ClientSayOnChatArgs]
        msg.unpack args
        sayOnChat c, args
    of clientSayToClient:       sayToClient c, unpacked.args
    else:                       discard

proc tryAcceptConnection =
    if connection.finished:
        if connection.failed: 
            connection = serverSocket.acceptAddr()
        else: 
            let c = Client(id: clientIdGen.getId)
            (c.address, c.socket) = connection.read
            clients.add c
            connection = serverSocket.acceptAddr()

proc tryReceiveMessage =
    for i, c in clients:
        if c.messageFuture.finished:
            if c.messageFuture.failed: 
                c.messageFuture = c.socket.recvLine
            else:
                processMessage c, c.messageFuture.read
                c.messageFuture = c.socket.recvLine

#mockup client

proc newMockup: MockupClient =
    let mc = MockupClient(socket: newAsyncSocket())
    asyncCheck mc.socket.connect("localhost", port)
    mockups.add mc
    mc

proc sendAs(mc: MockupClient, msg: SomeNetMsgArgs) {.async.} =
    mc.socket.send packNetMsg msg