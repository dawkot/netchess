import strutils, sequtils, math, hashes

{.this: self.}

type Matrix*[N, M: static[int], T: SomeNumber] = object
    xs*: array[N, array[M, T]]

proc hash*[N, M, T](self: Matrix[N, M, T]): Hash =
    for row in self.xs:
        for el in row:
            result = result !& hash(el)
    return !$(result)

proc matrix*[N, M: static[int], T](xs: array[N, array[M, T]]): Matrix[N, M, T] = 
    result.xs = xs

proc convertedTo*[N, M, T, T2](self: Matrix[N, M, T], typ: typedesc[T2]): Matrix[N, M, T2] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[x, y] = T2 self[x, y]

proc matrix*[N, M: static[int]; T](typ: typedesc[T], xs: array[N, array[M, SomeNumber]]): Matrix[N, M, T] =
    for y in 0 ..< N:
        for x in 0 ..< M:
            result.xs[y][x] = T xs[y][x]

proc identityMatrix*[T](X: static[int], typ: typedesc[T]): Matrix[X, X, T] =
    for i in 0 ..< X: 
        result.xs[i][i] = T 1

proc `[]`*[N, M, T](self: Matrix[N, M, T], x, y: int): T = 
    self.xs[y][x]

proc `[]=`*[N, M, T](self: var Matrix[N, M, T], x, y: int, el: T) = 
    self.xs[y][x] = el

iterator rows*[N, M, T](self: Matrix[N, M, T]): array[M, T] =
    for row in self.xs: yield row

iterator columns*[N, M, T](self: Matrix[N, M, T]): array[N, T] =
    for x in 0 ..< M:
        var column: array[N, T]
        for y in 0 ..< N:
            column[y] = self[x, y]
        yield column

proc `==`*[N, M, T](m1, m2: Matrix[N, M, T]): bool =
    result = true
    for x in 0 ..< M:
        for y in 0 ..< N:
            if m1[x, y] != m2[x, y]:
                return false

proc `$`*[N, M, T](self: Matrix[N, M, T]): string =
    result = ""
    for row_i, row in toSeq(self.rows):
        var row_repr = "[\t"
        for el_i, el in row:
            let last_row = row_i == N - 1
            let last_el = el_i == M - 1
            row_repr.add($el & (if last_row and last_el: "\t]." elif last_el: "\t]\n" else: "\t"))
        result &= row_repr

proc transposed*[N, M, T](self: Matrix[N, M, T]): Matrix[M, N, T] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[y, x] = self[x, y]

proc `+`*[N, M, T](self: Matrix[N, M, T], num: T): Matrix[N, M, T] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[x, y] = self[x, y] + num

template `+`*[N, M, T](num: T, self: Matrix[N, M, T]): Matrix[N, M, T] = 
    self + num

proc `-`*[N, M, T](self: Matrix[N, M, T], num: T): Matrix[N, M, T] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[x, y] = self[x, y] - num

proc `*`*[N, M, T](self: Matrix[N, M, T], num: T): Matrix[N, M, T] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[x, y] = self[x, y] * num

template `*`*[N, M, T](num: T, self: Matrix[N, M, T]): Matrix[N, M, T] = 
    self * num

proc `/`*[N, M, T](self: Matrix[N, M, T], num: T): Matrix[N, M, T] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[x, y] = self[x, y] / num

proc `+`*[N, M, T](m1, m2: Matrix[N, M, T]): Matrix[N, M, T] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[x, y] = m1[x, y] + m2[x, y]

proc `-`*[N, M, T](m1, m2: Matrix[N, M, T]): Matrix[N, M, T] =
    for x in 0 ..< M:
        for y in 0 ..< N:
            result[x, y] = m1[x, y] - m2[x, y]

proc `*`*[N, X, M, T](m1: Matrix[N, X, T], m2: Matrix[X, M, T]): Matrix[N, M, T] =
    for y, row in toSeq(m1.rows):
        for x, col in toSeq(m2.columns):
            for el_i in 0 ..< X:
                result[x, y] = result[x, y] + m1[el_i, y] * m2[x, el_i]

proc submatrix*[N, M, T](self: Matrix[N, M, T], X, Y, W, H: static[int]): Matrix[W, H, T] =
    for x in X ..< X+W:
        for y in Y ..< Y+H:
            result[x-X, y-Y] = self[x, y]

proc withRow*[N, M, T](self: Matrix[N, M, T], i: 0..N-1, new_row: array[M, T]): Matrix[N, M, T] =
    for row_i, row in toSeq(self.rows):
        if row_i == i: 
            result.xs[i] = new_row
        else: result.xs[i] = 
            self.xs[i]

proc withColumn*[N, M, T](self: Matrix[N, M, T], i: 0..M-1, new_col: array[N, T]): Matrix[N, M, T] =
    for col_i, col in toSeq(self.columns):
        if col_i == i :
            for el_i, el in new_col:
                result[col_i, el_i] = el
        else:
            for el_i, el in col:
                result[col_i, el_i] = el

    for el_i, el in new_col:
        result[i, el_i] = el

proc withoutRow_IMPLEMENTATION*[N, M, T](self: Matrix[N, M, T], i: 0..N-1, N2: static[int]): Matrix[N2, M, T] =
    var row_skipped = false
    for row_i, row in toSeq(self.rows):
        if row_i == i: row_skipped = true
        elif row_skipped: result.xs[row_i-1] = row
        else: result.xs[row_i] = row

template withoutRow*[N, M, T](self: Matrix[N, M, T], i: 0..N-1): untyped = 
    withoutRow_IMPLEMENTATION(self, i, N-1)

proc withoutColumn_IMPLEMENTATION*[N, M, T](self: Matrix[N, M, T], i: 0..M-1, M2: static[int]): Matrix[N, M2, T] =
    var col_skipped = false
    for col_i, col in toSeq(self.columns):
        if col_i == i: col_skipped = true
        elif col_skipped:
            for el_i, el in col:
                result[col_i-1, el_i] = el
        else:
            for el_i, el in col:
                result[col_i, el_i] = el

template withoutColumn*[N, M, T](self: Matrix[N, M, T], i: 0..M-1): untyped = 
    withoutColumn_IMPLEMENTATION(self, i, M-1)

proc det*[X, T](m: Matrix[X, X, T]): int =
    when X == 2:
        int( m[0, 0] * m[1, 1] - m[1, 0] * m[0, 1] )
    elif X == 3:
        int ( m[0, 2] * m[1, 1] * m[2, 0] + 
            m[0, 1] * m[1, 0] * m[2, 2] + 
            m[0, 0] * m[1, 2] * m[2, 1] -
            m[0, 0] * m[1, 1] * m[2, 2] - 
            m[1, 0] * m[2, 1] * m[0, 2] - 
            m[2, 0] * m[0, 1] * m[1, 2] )
    else:
        let m_without_row = m.withoutRow(0)
        for el_i, el in toSeq(m.rows)[0]:
            let sub = m_without_row.withoutColumn(el_i)
            result += int( T( (-1)^(el_i+1) ) * el * T( det(sub) ) )

proc inversed*[X; T: SomeReal](m: Matrix[X, X, T]): Matrix[X, X, T] =    
    var m_d: Matrix[X, X, T]
    for x in 0 ..< X:
        for y in 0 ..< X:
            let sub = m.withoutColumn(x).withoutRow(y)
            m_d[x, y] = T( (-1)^(x+y+1) * det(sub) )
    (1 / det(m)) * transposed(m_d)

proc `^`*[X; T: SomeReal](m: Matrix[X, X, T], i: static[1..1]): Matrix[X, X, T] = 
    inversed(m)

proc `/`*[N, M, T](m1, m2: Matrix[N, M, T]): Matrix[N, M, T] =
    m1 * inversed(m2)

type Vector*[I: static[int], T] = Matrix[1, I, T]

proc `[]`*[I, T](self: Vector[I, T], i: 0..(I-1)): T =
    self[0, i]

converter toVector*[I, T](self: Matrix[I, 1, T]): Vector[I, T] =
    for i, x in self.xs:
        let el = x[0]
        result[i] = el

proc vec*[I: static[int], T](xs: array[I, T]): Vector[I, T] = result.xs = [xs]
proc vec2*[T](x, y: T): Vector[2, T] = result.xs = [[x, y]] 
proc vec3*[T](x, y, z: T): Vector[3, T] = result.xs = [[x, y, z]]

iterator items*[I, T](self: Vector[I, T]): T =
    for el in self.xs[0]: yield el

iterator mitems*[I, T](self: var Vector[I, T]): var T =
    for el in self.xs[0]: yield el

proc `x`*[T](self: Vector[2, T] or Vector[3, T]): T = self[0]
proc `y`*[T](self: Vector[2, T] or Vector[3, T]): T = self[1]
proc `z`*[T](self: Vector[2, T] or Vector[3, T]): T = self[2]

proc `x`*[T](self: var (Vector[2, T] or Vector[3, T])): var T = self[0]
proc `y`*[T](self: var (Vector[2, T] or Vector[3, T])): var T = self[1]
proc `z`*[T](self: var (Vector[2, T] or Vector[3, T])): var T = self[2]

proc `x=`*[T](self: var (Vector[2, T] or Vector[3, T]), val: T) = self[0] = val
proc `y=`*[T](self: var (Vector[2, T] or Vector[3, T]), val: T) = self[1] = val
proc `z=`*[T](self: var (Vector[2, T] or Vector[3, T]), val: T) = self[2] = val

proc converted*[I, T; T2: SomeNumber](v: Vector[I, T], new_type: typedesc[T2]): Vector[I, T2] =
    for i in 0 ..< I:
        result[i] = T2 v[i]

proc `$`*[T](v: Vector[2, T]): string = 
    "Vector2(x: $1, y: $2)" % [$v.x, $v.y]

proc `$`*[T](v: Vector[3, T]): string = 
    "Vector3(x: $1, y: $2, z: $3)" % [$v.x, $v.y, $v.z]

proc `$`*[I, T](v: Vector[I, T]): string =
    result = "Vector$1(" % $I
    let values = v.xs[0]
    for i, x in v.values:
        result.add: "$1: $2" % [$i, $x]
        let last_iter = ( i == v.values.len - 1 )
        if not last_iter:
            result.add(", ")
    result.add(")")

iterator items*[I, T](v: Vector[I, T]): T =
    for val in v.xs[0]:
        yield val

iterator mitems*[I, T](v: var Vector[I, T]): var T =
    for val in v.xs[0]:
        yield val

iterator pairs*[I, T](v: Vector[I, T]): (int, T) =
    for i, val in v.xs[0]:
        yield(i, val)

iterator mpairs*[I, T](v: var Vector[I, T]): (int, var T) =
    for i, val in v.xs[0]:
        yield(i, val)

proc `+`*[I, T](a, b: Vector[I, T]): Vector[I, T] =
    for i in 0 ..< I: 
        result[i] = a[i] + b[i]

proc `+=`*[I, T](a: var Vector[I, T], b: Vector[I, T]) =
    for i in 0 ..< I:
        a[i] += b[i]

proc `-`*[I, T](a, b: Vector[I, T]): Vector[I, T] =
    for i in 0 ..< I: 
        result[i] = a[i] - b[i]

proc `*`*[I, T](v: Vector[I, T], val: T): Vector[I, T] =
    for i in 0 ..< I:
        result[i] = v[i] * val

template `*`*[I, T](val: T, v: Vector[I, T]): Vector[I, T] =
    v * val

proc `/`*[T: SomeReal, I](v: Vector[I, T], val: T): Vector[I, T] =
    for i in 0 ..< I:
        result[i] = v[i] / val

proc `div`*[T: SomeInteger, I](v: Vector[I, T], val: T): Vector[I, T] =
    for i in 0 ..< I:
        result[i] = v[i] div val

proc len*[I, T; F: SomeReal](v: Vector[I, T], return_type: typedesc[F]): F =
    var sum: T
    for val in v: 
        sum += val^2
    sqrt(F sum)

template len*[I, T](v: Vector[I, T]): float =
    len(v, float)

proc normalized*[T: SomeReal, I](v: Vector[I, T]): Vector[I, T] =
    let len = v.len
    for i, val in v:
        result[i] = val / len