import sequtils, tables, matrixes, future, rpcs, options,
    asyncdispatch, asyncnet, strutils, os, msgpack4nim, hashes
include chess

{.this: self.}

#[
    rpc table should be a part of the server instead (using a generic)
]#

type
    Client = ref object
        server: Server
        socket: AsyncSocket
        player: Option[Player]
        name: string
        next_message: Future[string]

    Server = ref object
        match: Match
        socket: AsyncSocket
        players: array[Player, Client]
        clients: seq[Client]
        next_connection: Future[AsyncSocket]
        password_hash: Hash
        sender_of_last_msg: Client
        rpcs: RpcTable

#utils

proc toString(xs: seq[char]): string =
    result = newStringOfCap(xs.len)
    for x in xs:
      result.add(x)

template withValue[T](val: Option[T], ident, body: untyped): untyped =
    if val.isSome:
        let ident {.inject.} = val.get
        body

#core

proc newServer(password_hash: Hash, port: Port, address = ""): Server =
    let s = result
    s.match = Match()
    s.clients = @[]
    s.socket = newAsyncSocket()
    s.socket.setSockOpt(OptReuseAddr, true)
    s.socket.bindAddr(port, address)
    s.socket.listen()
    s.next_connection = s.socket.accept()

proc pollNewConnections(self: Server) =
    while next_connection.finished:
        if not next_connection.failed:
            let client_socket = next_connection.read()
            clients.add Client(
                server: self,
                socket: client_socket,
                next_message: client_socket.recvLine(),
                player: none(Player),
                name: "undefined"
            )
        next_connection = socket.accept()

proc pollNewMessages(self: Server) =
    for c in clients:
        while c.next_message.finished:
            if not c.next_message.failed:
                discard
                #processMessage(c, c.next_message.read)
            c.next_message = c.socket.recvLine()

var current_server: Server

const MoveRpc = hash "move"
const PiecesRpc = hash "pieces"

#received rpcs

proc rpc_register(name: seq[char], player: Option[Player]): bool =
    let s = current_server
    let c = s.sender_of_last_msg
    player.withValue(player):
        if s.players[player] == nil:
            s.players[player] = c
            c.player = some(player)
            c.name = toString(name)
            return true

proc rpc_move(origin, dest: Position): bool =
    let s = current_server
    let c = s.sender_of_last_msg
    let pieces = box(s.match.pieces)
    let validated = 
        s.players[s.match.current_player] == c and 
        valid(dest) and 
        valid(origin) and
        pieces.hasKey(dest) and
        pieces.hasKey(origin) and
        dest in pieces[dest].possiblePositions
    if validated:
        let piece = box(pieces[origin])
        piece.pos = dest
        pieces[dest] = piece[]
        for other_c in s.clients.filter((client) => client == c):
            asyncCheck: other_c.socket.send(pack((MoveRpc, origin, dest)) & "\n")
        true
    else:
        false

when isMainModule:
    let password = paramStr(1)
    let port = Port parseInt(paramStr(2))
    let address = 
        if paramCount() >= 2: paramStr(3) 
        else: ""
    let server = newServer(hash(password), port, address)
