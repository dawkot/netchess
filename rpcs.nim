import tables, sequtils, strutils, macros
import msgpack4nim

export tables, msgpack4nim

{.this: self.}

proc unpack*(msg: string, T: typedesc): T =
    msg.unpack(result)

type RpcTable* = ref object
    table*: TableRef[int, proc(rpc_table: RpcTable, caller_id: int, msg: string)]
    on_invalid_message*: proc(caller_id: int, msg: string)
    on_response_request*: proc(caller_id: int, msg: string)

proc newRpcTable*: RpcTable = RpcTable(
    table: newTable[int, proc(rpc_table: RpcTable, caller_id: int, msg: string)](),
    on_invalid_message: proc(caller_id: int, msg: string) = discard,
    on_response_request: proc(caller_id: int, msg: string) = discard
)

macro register*(rpc_table: RpcTable, rpc_id: int, rpc_cb: proc): untyped =
    var rpc_arg_types_temp: seq[NimNode] = @[]

    echo rpc_cb.kind
    if rpc_cb.kind != nnkSym:
        error("Lambdas are not supported because repr() doens't work correctly")

    echo treeRepr(rpc_cb)

    if rpc_cb.kind == nnkSym:
        for i in 1 ..< rpc_cb.symbol.getImpl[3].len:
            let arg_defs = rpc_cb.symbol.getImpl[3]
            var idents = 0
            var j = 0
            for x in arg_defs[i]:
                if x.kind == nnkIdent: inc(idents)
                if x.kind == nnkSym:
                    for z in 1 .. idents: rpc_arg_types_temp.add(argDefs[i][j])
                    idents = 0
                inc(j)

    else:
        for i in 1 ..< rpc_cb[3].len:
            let arg_defs = rpc_cb[3]
            var idents = 0
            var j = 0
            for x in arg_defs[i]:
                if x.kind == nnkIdent: inc(idents)
                if x.kind == nnkSym:
                    for z in 1 .. idents: rpc_arg_types_temp.add(argDefs[i][j])
                    idents = 0
                inc(j)

    let 
        cb_return_type = 
            if rpc_cb.kind == nnkSym:
                if rpc_cb.symbol.getImpl[3][0] == newEmptyNode(): ident("void")
                else: rpc_cb.symbol.getImpl[3][0]
            else:
                if rpc_cb[3][0] == newEmptyNode(): ident("void")
                else: rpc_cb[3][0]

        cb_arg_types =
            if rpc_arg_types_temp.len > 0:  rpc_arg_types_temp
            else:                           @[]

        msg_cast_type = cb_arg_types.mapIt($it.repr).join(", ")

    var f_call_arg_list = ""
    
    var i = if cb_return_type == ident("void"): 1 else: 2
    for j in 1 .. cb_arg_types.len:
        f_call_arg_list.add "x[$1]" % $i
        if j != cb_arg_types.len: f_call_arg_list.add ", "
        inc i

    var proc_def = "proc(rpc_table: RpcTable, caller_id: int, msg: string) =\n"
    
    dumpTree:
        let my_lambda = proc = echo ":)"

    if cb_return_type != ident("void") and cb_arg_types.len != 0:
        proc_def.add: "    defer: rpc_table.on_invalid_message(c, msg)\n"

    if cb_arg_types.len == 0:
        if cb_return_type == ident("void"):
            proc_def.add: "    $1()\n" % $rpc_cb
        else:
            proc_def.add: "    let x = msg.unpack((int, int))\n" 
            proc_def.add: "    let result = $1()\n" % $rpc_cb

    elif cb_arg_types.len == 1:
        if cb_return_type == ident("void"):
            proc_def.add: "    let x = msg.unpack((int, $1))\n" % msgCastType
            proc_def.add: "    $1(x[1])\n" % $rpc_cb
        else:
            proc_def.add: "    let x = msg.unpack((int, int, $1))\n" % msgCastType
            proc_def.add: "    let result = $1(x[2])\n" % $rpc_cb

    elif cb_arg_types.len >= 2:
        if cb_return_type == ident("void"):
            proc_def.add: "    let x = msg.unpack((int, $1))\n" % msgCastType
            proc_def.add: "    $1($2)\n" % [$rpc_cb, f_call_arg_list]
        else:
            proc_def.add: "    let x = msg.unpack((int, int, $1))\n" % msgCastType
            proc_def.add: "    let result = $1($2)\n" % [$rpc_cb, f_call_arg_list]
        
    if cb_return_type != ident("void"):
        proc_def.add: "    let request_id = x[1]\n" % $rpc_cb
        proc_def.add: "    rpc_table.on_response_request(caller_id, (request_id, result).pack)\n"
    
    let code = "$1.table[$2] = $3\n" % [$rpc_table, $rpc_id.repr, $proc_def]
    when defined(debug_rpcs): 
        echo code
    parseStmt code

proc call*(self: RpcTable, caller_id: int, msg: string) =
    try:
        let key = int: msg.toAny.arrayVal[0].intVal
        if table.hasKey(key):
            table[key](self, caller_id, msg)
        else:                               
            on_invalid_message(caller_id, msg)
    except:
        on_invalid_message(caller_id, msg)

template call*(self: RpcTable, caller_id: int, args: tuple | array) =
    call(self, caller_id, msgpack4nim.pack(args))

when isMainModule:
    let table = newRpcTable()
    
    var x: int
    proc prc = x = 2
    table.register(2, prc)
    table.call(0, pack([2]))
    echo x

    proc myProc(a, b: string) = echo a & " " & b
    table.register(1, myProc)
    table.call(666, pack((1, "hello", "world")))
    table.call(666, (1, "hello", "world"))
    
    proc myProc2: string = ":^)"
    table.on_response_request = proc(caller_id: int, msg: string) = echo msg.unpack((int, string))
    table.register(2, myProc2)
table.call(666, (2, 8))