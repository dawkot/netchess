import sequtils, tables, matrixes, future

{.this: self.}

type
    Position = Vector[2, int]
    Player = enum a, b
    PieceKind = enum pawn, rook, bishop, knight, king, queen

    Piece = object
        match: Match
        kind: PieceKind
        pos: Position
        owner: Player

    Match = ref object
        pieces: Table[Position, Piece]
        current_player: Player

#utils

proc box[T](val: T): ref T =
    new result
    result[] = val

proc obtain[T](xs: openArray[T], cond: proc(x: T): bool {.closure.}): T =
    for x in xs:
        if cond(x): 
            return x
    raise newException(KeyError, 
        "No element of the collection conformed to the condition")

#core

proc valid(pos: Position): bool =
    pos.x in 1..8 and pos.y in 1..8

proc init(self: Match) =
    current_player = a
    for x in 1..8:
        for y in [1, 2, 7, 8]:
            let owner =
                if y in [1, 2]: a
                else:           b
            let kind =
                if y in [1, 8]:
                    case (range[1..8])(x)
                    of 1, 8: rook
                    of 2, 7: knight
                    of 3, 6: bishop
                    of 4: king
                    of 5: queen
                else:
                    pawn
            let pos = vec2(x, y)
            pieces[pos] = Piece(kind: kind, owner: owner, pos: pos)

proc possiblePositions(self: Piece): seq[Position] =
    result = @[]
    let pieces = box(match.pieces)
    case kind
    of pawn:
        let forward_pos = pos + vec2(0, 1)
        if not pieces.hasKey(forward_pos):
            result.add(forward_pos)
        for vec in [vec2(1, 1), vec2(-1, 1)]:
            let diagonal_pos = pos + vec
            pieces[].withValue(diagonal_pos, other):
                if other.owner != self.owner:
                    result.add(diagonal_pos)
    of rook, bishop, queen:
        let modifiers =
            case kind
            of rook: @[vec2(1, 0), vec2(-1, 0), vec2(0, 1), vec2(0, -1)]
            of bishop: @[vec2(1, 1), vec2(-1, -1), vec2(-1, 1), vec2(1, -1)]
            else: @[vec2(1, 0), vec2(-1, 0), vec2(0, 1), vec2(0, -1), vec2(1, 1), vec2(-1, -1), vec2(-1, 1), vec2(1, -1)]
        for vec in modifiers:
            var cur_pos = pos
            while true:
                cur_pos += vec
                if not valid(cur_pos):
                    break
                if pieces.hasKey(cur_pos):
                    let other = pieces[cur_pos]
                    if other.owner != self.owner: 
                        result.add(cur_pos)
                    break
                else:
                    result.add(cur_pos)
    of knight, king:
        let modifiers =
            if kind == knight: @[vec2(2, 1), vec2(-2, 1), vec2(2, -1), vec2(-2, -1), vec2(1, 2), vec2(1, -2), vec2(-1, 2), vec2(-1, -2)]
            else: @[vec2(1, 0), vec2(-1, 0), vec2(0, 1), vec2(0, -1), vec2(1, 1), vec2(-1, -1), vec2(1, -1), vec2(-1, 1)]
        for vec in modifiers:
            let cur_pos = pos + vec
            pieces[].withValue(cur_pos, other):
                if other.owner != self.owner:
                    result.add(cur_pos)

proc checkmateBy(self: Match, player_who_just_finished: Player): bool =
    let king = 
        toSeq(pieces.values)
        .obtain((piece) => piece.kind == king and piece.owner != player_who_just_finished)
    let king_alts = king.possiblePositions
    toSeq(pieces.values)
        .filter((piece) => piece.owner == player_who_just_finished)
        .all((piece) => piece.possiblePositions.any((pos) => pos in king_alts))
